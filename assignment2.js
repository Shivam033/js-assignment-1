// console.log("Connected!");

class Person{
    constructor(name, age, salary, sex){
        this.name = name;
        this.age = age;
        this.salary = salary;
        this.sex = sex;
    }
    static sorted(val, param, order){
        let arr = quickSort(val, param);
        if(order == 'asc'){
            return arr;
        }else if(order == 'desc'){
            return arr.slice().reverse();
        }else{
            return "Invalid order."
        }
    }
}
class Persons{
    constructor(){
        this.persons = []
    }
    newPerson(name, age, salary, sex){
        let p = new Person(name, age, salary, sex);
        this.persons.push(p);
        // return p;
    }

}

let arrayed = new Persons();
// Add new persons here
arrayed.newPerson("Adam", 23, 80000, "male");
arrayed.newPerson("Boby", 25, 60000, "female");
arrayed.newPerson("Dave", 18, 20000, "male");

let arr = arrayed.persons;

// Answer
// change val(arr), parameter('name') and order('asc') values here
let answer = Person.sorted(arr, 'name', 'desc'); 
console.log(answer);


function quickSort(arr, para){
    if(arr.length <=1){
        return arr;
    }else{
        let lftArr = [];
        let rtArr = [];
        let newArr = [];

        let pivot = arr.pop();
        let len = arr.length;
        
        if(para == "age"){
            for(let i=0; i<len; i++){
                if(arr[i].age <= pivot.age){
                    lftArr.push(arr[i]);
                }else{
                    rtArr.push(arr[i]);
                }
            }
            return newArr.concat(quickSort(lftArr, 'age'), pivot, quickSort(rtArr, 'age'));

        }else if(para == 'salary'){
            for(let i=0; i<len; i++){
                if(arr[i].salary <= pivot.salary){
                    lftArr.push(arr[i]);
                }else{
                    rtArr.push(arr[i]);
                }
            }
            return newArr.concat(quickSort(lftArr, 'salary'), pivot, quickSort(rtArr, 'salary'));
        }else if(para == 'name'){
            for(let i=0; i<len; i++){
                if(arr[i].name <= pivot.name){
                    lftArr.push(arr[i]);
                }else{
                    rtArr.push(arr[i]);
                }
            }
            return newArr.concat(quickSort(lftArr, 'name'), pivot, quickSort(rtArr, 'name'));
        }else{
            return console.log("Invalid sorting field.");
        }

    }
}
