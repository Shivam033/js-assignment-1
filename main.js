// console.log('Connected');

let email = document.querySelector("#email");
let password = document.querySelector("#pswrd");


let admin = document.querySelector("#admin");
let user = document.querySelector("#user");
let role; 

let p1 = document.querySelector("#perm1");
let p2 = document.querySelector("#perm2");
let p3 = document.querySelector("#perm3");
let p4 = document.querySelector("#perm4");
let perms;

let sex = document.querySelector("#gender");

let form = document.querySelector("#formElement");

function submitEvent(event){
    event.preventDefault();
   
    validateEmail(email.value);
    validatePassword(password.value);
    handleRole(admin, user);
    handlePermissions(p1, p2, p3, p4);

    if(validateEmail(email.value) && validatePassword(password.value) && handleRole(admin, user) && handlePermissions(p1, p2, p3, p4)){
        cnfrm();
        form.remove();
    };

    // cnfrm();

    console.log(email.value);
    console.log(password.value);
    console.log(gender.value);
    console.log(role);
    console.log(perms);
    

    email.value = "";
    password.value = "";

    role = "";
    admin.checked = false;
    user.checked = false;

    p1.checked = false;
    p2.checked = false;
    p3.checked = false;
    p4.checked = false;
    perms = "";

    gender.value = "male";

}

function cnfrm(){
    confirm(`Email : ${email.value}
    Password : ${password.value}
    Sex : ${gender.value}
    Role : ${role}
    Permissions = ${perms}
    Confirm Data?`)
}

// -------------------------------EMAIL
function validateEmail(inputText){
    var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

    if(inputText === ""){

        alert("Email address cannot be empty!");
        return false;
    }else if (!inputText.match(mailformat)){

        alert("You have entered an invalid email address!");
        return false;
    }
    return true;
}

// ------------------------------PASSWORD
function validatePassword(pswrd){
    // password between 6 to 20 characters which contain at least one  
    // numeric digit, one uppercase and one lowercase letter
    var passw=  /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$/;

    if(pswrd === ""){

        alert("Password cannot be empty!");
        return false;
    }else if (!pswrd.match(passw)){

        alert("You have entered an invalid password!");
        return false;
    }
    return true;
}

// ----------------------------- ROLE
function handleRole(admn, usr){
    if(admn.checked == false && usr.checked == false){
        alert("Please select a role!")
        return false;
    }else if(admn.checked == true){
        role = admn.value;
        return true;
    }else{
        role = usr.value;
        return true;
    }
}

// --------------------------PERMISSIONS
function handlePermissions(pr1, pr2, pr3, pr4){
    if(pr1.checked == true && pr2.checked == true && pr3.checked == false && pr4.checked == false){
        perms = "Permission 1 & Permission 2"
        return true;
    }else if(pr1.checked == true && pr3.checked == true && pr2.checked == false && pr4.checked == false){
        perms = "Permission 1 & Permission 3"
        return true;
    }else if(pr1.checked == true && pr4.checked == true && pr2.checked == false && pr3.checked == false){
        perms = "Permission 1 & Permission 4"
        return true;
    }else if(pr2.checked == true && pr3.checked == true && pr1.checked == false && pr4.checked == false){
        perms = "Permission 2 & Permission 3"
        return true;
    }else if(pr2.checked == true && pr4.checked == true && pr3.checked == false && pr1.checked == false){
        perms = "Permission 2 & Permission 4"
        return true;
    }else if(pr3.checked == true && pr4.checked == true && pr1.checked == false && pr2.checked == false){
        perms = "Permission 3 & Permission 4"
        return true;
    }else if(pr1.checked == true && pr2.checked == true && pr3.checked == true && pr4.checked == false){
        perms = "Permission 1, Permission 2 & Permission 3"
        return true;
    }else if(pr4.checked == true && pr2.checked == true && pr3.checked == true && pr1.checked == false){
        perms = "Permission 2, Permission 3 & Permission 4"
        return true;
    }else if(pr1.checked == true && pr2.checked == true && pr4.checked == true && pr3.checked == false){
        perms = "Permission 1, Permission 2 & Permission 4"
        return true;
    }else if(pr1.checked == true && pr4.checked == true && pr3.checked == true && pr2.checked == false){
        perms = "Permission 1, Permission 3 & Permission 4"
        return true;
    }else if(pr1.checked == true && pr2.checked == true && pr3.checked == true && pr4.checked == true){
        perms = "All Permissions are selected!"
        return true;
    }else{
        alert("Please select at least 2 premissions.")
        return false;
    }
}